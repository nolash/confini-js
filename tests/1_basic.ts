import * as assert from 'assert';

import { Config } from '../src/config';

describe('basic', () => {
	it('process', () => {
		const c = new Config(__dirname + '/testdata');
		c.process();
		assert.equal(c.get('FOO_BAR'), 'baz');
		assert.equal(c.get('BAZ_XYZZY'), 666);
	});

	it('override_simple', () => {
		const c = new Config(__dirname + '/testdata');
		c.process();
		const o = {
			'FOO_BAR': 42,
		}
		c.override(o);
		assert.equal(c.get('FOO_BAR'), 42);
	});

	it('override_prefix', () => {
		const c = new Config(__dirname + '/testdata');
		c.process();
		const o = {
			'XXX_FOO_BAR': 42,
		}
		c.override(o, 'XXX_');
		assert.equal(c.get('FOO_BAR'), 42);

	});
});
